//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <test_plugin/test_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  TestPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("TestPlugin"));
}
